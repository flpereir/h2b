"""Convert hd5 data to brilcalc csv format.

This script is used to convert hd5 data to brilcalc csv format. It is meant to be used
solely for development purposes and does not try to invalidate the use of the official
brilcalc tool.

The following approach is used to convert the hd5 data to brilcalc csv format:

1. Compute all paths of interest from the given input: The user can specify a single file,
a folder or a central. Based on the input, all paths to the hd5 files are computed.

2. Create a map from run number to (ls_mask, paths): For each run, use the provided normtag
to compute the ls_mask and the paths to the hd5 files. Store the result in a dictionary for
later use.

3. Iterate all key value pairs and:
    a. At the path level, filter all the STABLE BEAMS entries.
    b. Apply lsmask to result from (a).
    c. Concatenate all the resulting dataframes from step (b).
    d. Eliminate any duplicates in the timestampsec columns.
    e. Perform the aggregation over run-lumisection, column matching
    f. Append the dataframe to the csv
"""
from typing import Dict, List, Union, Tuple, Optional, Iterable, TypeVar
from collections import defaultdict
from enum import Enum, auto
from pathlib import Path

import re
import sys
import json
import logging
import argparse

from tqdm import tqdm
from tables.exceptions import NoSuchNodeError

import yaml
import tables
import numpy as np
import pandas as pd


A = TypeVar('A')
B = TypeVar('B')
NormTagSchema = List[List[Union[str, Dict[str, List[List[int]]]]]]

logger: logging.Logger = None


class InputType(Enum):
    FILE = auto()
    FOLDER = auto()
    CENTRAL = auto()


class IovTag:
    def __init__(self, name: str):
        self.name = name

        with open(f"/brildata/db/{name}.yaml") as file:
            data: List[Dict[str, Dict[str, str]]] = yaml.safe_load(file)["since"]

        self.run_to_coefs = self._compute_run_to_coefs(data)

    def get_coefs(self, run: int) -> np.ndarray:
        """Get the coefs for the given run.

        Parameters
        ----------
        run : int
            The run number for which to get the coefs.

        Returns
        -------
        np.ndarray
            The coefs for the given run.

        Raises
        ------
        ValueError
            If the run is not present in any of the ranges.
        """
        for range, coefs in self.run_to_coefs.items():
            if run in range:
                return coefs
        raise ValueError(f"Could not find coefs for run {run}")

    def _compute_run_to_coefs(self, data: List[Dict[str, Dict[str, str]]]) -> Dict[range, np.ndarray]:
        """Compute the mapping from run to iovtag coefs.

        Each element in the data list is a dictionary where the key is the run number and the value
        is another dictionary with only one key of interest: "payload". The payload dictionary contains
        a key "coefs". An example of the data list is:

        .. code-block:: yaml

            - 365748:
                func: poly1d
                payload: {'coefs': '-16.6889,40.892949,0.'}
                comment: 'This is a comment'
            - 365758:

        This function will return a dictionary where the keys are ranges of run numbers and the values
        are the coefs for the iovtag. Considering the example above, one of the keys would be:

        .. code-block:: python

            range(365748, 365758) : np.array([-16.6889, 40.892949, 0.])

        This entails that the coefs are valid for all runs in the range 365748 to 365757. However, the
        iovtags has been known to contain bugs where the range is not correct. Such bugs would manifest
        as 0-length ranges. This function handles such cases by ignoring these 0-length ranges but keeping
        the coefs.
        """
        i = 0
        run_to_coefs: Dict[range, np.ndarray] = {}
        # Index iteration in order to ignore 0-length ranges
        while i < len(data):
            entry = data[i]
            run = int(list(entry.keys())[0])
            coefs = np.fromstring(list(entry.values())[0]["payload"]["coefs"], dtype=np.float, sep=',')
            if i + 1 < len(data):
                next_run = int(list(data[i + 1].keys())[0])
                # While the next run is wuold produce a 0-length range, keep iterating
                while i + 1 < len(data) and run == next_run:
                    i += 1
                    next_run = int(list(data[i + 1].keys())[0])
                run_to_coefs[range(run, next_run)] = coefs
            else:
                run_to_coefs[range(run, sys.maxsize)] = coefs
            i += 1
        return run_to_coefs

class NormTag:
    def __init__(self, path: Path):
        self.name = path.name

        with path.open("r") as file:
            data: NormTagSchema = json.load(file)

        self.run_to_lsmask, self.run_to_iovtag = self._aggregate_data(data)

    def __contains__(self, run: int) -> bool:
        return run in self.run_to_lsmask

    def _aggregate_data(self, normtag_data: NormTagSchema) -> Tuple[Dict[int, str], Dict[int, IovTag]]:
        seen_iovtags = set()
        existing_iovtags = {}
        run_to_lsmask: Dict[int, str] = {}
        run_to_iovtag: Dict[int, IovTag] = {}
        for entry in normtag_data:
            iovtag_name: str = entry[0]
            run_dict: Dict[str, List[List[int]]] = entry[1]

            if iovtag_name not in seen_iovtags:
                seen_iovtags.add(iovtag_name)
                existing_iovtags[iovtag_name] = IovTag(iovtag_name)

            iovtag = existing_iovtags[iovtag_name]

            for run, ls_ranges in run_dict.items():
                num_run = int(run)

                run_to_iovtag[num_run] = iovtag

                lsmask = self._compute_lsmask(ls_ranges)
                if num_run in run_to_lsmask:
                    run_to_lsmask[num_run] += f" or {lsmask}"
                else:
                    run_to_lsmask[num_run] = lsmask

        return run_to_lsmask, run_to_iovtag

    def _compute_lsmask(self, ls_ranges: List[List[int]]) -> str:
        lsmask = f"(lsnum >= {ls_ranges[0][0]} and lsnum <= {ls_ranges[0][1]})"
        for ls_range in ls_ranges[1:]:
            lsmask += f" or (lsnum >= {ls_range[0]} and lsnum <= {ls_range[1]})"
        return lsmask

def _fill_and_run_from_path(path: Path) -> Tuple[int, int]:
    mtch = re.match(r"(\d+)_(\d+)_.*", path.name)

    if mtch is None:
        raise ValueError(f"Could not extract run and fill from {path}")
    
    return int(mtch.group(1)), int(mtch.group(2))

def _read_hd5(path: Path, node: str, required_columns: List[str]) -> pd.DataFrame:
    with tables.open_file(path, "r") as t:
        node = t.get_node(f"/{node}")

        data = {col: node.col(col) for col in required_columns}

        return pd.DataFrame(data)

def read_hd5(path: Path, node: str) -> pd.DataFrame:
    """Read the hd5 file and return the data from the given node.

    A pre-defined set of columns will be read from the hd5 file. The columns are defined
    in the function and are required for the conversion to brilcalc csv format.

    Parameters
    ----------
    path : Path
        The path to the hd5 file.
    node : str
        The node from which to read the data.

    Returns
    -------
    pd.DataFrame
        The data from the hd5 file.
    """
    fill, run = _fill_and_run_from_path(path)

    if node == "beam":
        required_columns = ["lsnum", "timestampsec", "status", "egev", "ncollidable"]
    else:
        required_columns = ["lsnum", "timestampsec", "avgraw"]

    data = _read_hd5(path, node, required_columns)

    data["runnum"] = run
    data["fillnum"] = fill

    return data


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog="hd5_to_brilcalc_csv",
        description="Hd5 to brilcalc csv converter",
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    meg = parser.add_mutually_exclusive_group(required=True)
    meg.add_argument(
        "--file",
        type=Path,
        help="Path to the hd5 file to read and convert.",
    )
    meg.add_argument(
        "--folder",
        type=Path,
        help="Path to the folder containing the hd5 files to read and convert.",
    )
    meg.add_argument(
        "--central",
        type=Path,
        help="Path to the central folder containing the hd5 files to read and convert."
            " A central folder is any like: <central>/<fill-number>/<hd5-files>",
    )

    meg = parser.add_mutually_exclusive_group(required=True)
    meg.add_argument(
        "--beam-file",
        type=Path,
        help="Path to the beam file to use for the conversion. Required when using --file.",
    )
    meg.add_argument(
        "--beam-folder",
        type=Path,
        help="Path to the folder containing the beam files to use for the conversion. Required when using --folder.",
    )
    meg.add_argument(
        "--beam-central",
        type=Path,
        help="Path to the central folder containing the beam files to use for the conversion."
            " A central folder is any like: <central>/<fill-number>/<hd5-files-with-beam-information>"
            " Required when using --folder.",
    )

    parser.add_argument(
        "--node",
        type=str,
        metavar="NODE",
        required=True,
        help="Name of the node to read from the hd5 files.",
    )

    parser.add_argument(
        "--normtag",
        type=Path,
        metavar="NORMTAG",
        required=True,
        help="Path to the normtag file to use."
    )

    parser.add_argument(
        "--begin",
        type=int,
        metavar="FILL",
        help="Specify from which fill you want to consider. Only effective when using --central.",
    )

    parser.add_argument(
        "--end",
        type=int,
        metavar="FILL",
        help="Specify up to which fill you want to consider. Only effective when using --central.",
    )

    parser.add_argument(
        "--agg",
        type=int,
        default=1,
        metavar="INT",
        help="How many dataframes to aggregate before saving to file. It can improve performance by limiting writting to file.",
    )

    parser.add_argument(
        "--log-to-file",
        action="store_true",
        help="Log to file.",
    )

    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Set verbose log output."
    )    

    parser.add_argument(
        "-o", "--output",
        type=Path,
        metavar="",
        default=Path("output.csv"),
        help="Path to the output csv file.",
    )

    return parser


def _compute_paths_file(file: Path, beam_file: Path) -> Dict[int, List[Tuple[Path, Path]]]:
    return {_fill_and_run_from_path(file)[1]: [(file, beam_file)]}

def _compute_paths_folder(folder: Path, beam_folder: Path) -> Dict[int, List[Tuple[Path, Path]]]:
    result: Dict[int, List[Tuple[Path, Path]]] = defaultdict(list)
    for path in folder.glob("*.hd5"):
        result[_fill_and_run_from_path(path)[1]].append((path, beam_folder / path.name))
    return result

def _compute_paths_central(central: Path, beam_central: Path, begin: Optional[int] = None, end: Optional[int] = None) -> Dict[int, List[Tuple[Path, Path]]]:
    begin = begin or 0
    end = end or sys.maxsize

    result: Dict[int, List[Tuple[Path, Path]]] = defaultdict(list)
    for fill in central.iterdir():
        if not fill.is_dir():
            continue

        fill_number = int(fill.name)
        if  not (begin <= fill_number <= end):
            continue

        beam_central_fill = beam_central / fill.name
        for path in fill.glob("*.hd5"):
            result[_fill_and_run_from_path(path)[1]].append((path, beam_central_fill / path.name))

    return result

def compute_run_to_paths(input_type: InputType, args: argparse.Namespace) -> Dict[int, List[Tuple[Path, Path]]]:
    """Compute the mapping from run number to the hd5 file paths.

    Parameters
    ----------
    input_type : InputType
        The type of input the user has provided.
    args : argparse.Namespace
        The parsed arguments.

    Returns
    -------
    Dict[int, List[Tuple[Path, Path]]]
        The mapping from run number to the hd5 file paths.
    """
    if input_type == InputType.FILE:
        return _compute_paths_file(args.file, args.beam_file)
    elif input_type == InputType.FOLDER:
        return _compute_paths_folder(args.folder, args.beam_folder)
    elif input_type == InputType.CENTRAL:
        return _compute_paths_central(args.central, args.beam_central, args.begin, args.end)
    else:
        raise ValueError(f"Unknown input type: {input_type}")


def _name_from_node(node: str) -> str:
    name = re.match(r"(scan5_)?(\w+)lumi.*", node).groups()[1].upper()
    if name == "PLT":
        return "PLTZERO"
    return name

def _find_best_roll(data: pd.DataFrame, beam: pd.DataFrame) -> pd.DataFrame:
    i = 0
    min_diff = sys.maxsize

    for j in range(2):
        diff = (data["timestampsec"].iloc[j:] - beam["timestampsec"]).abs().max()
    
        if diff < min_diff:
            min_diff = diff
            i = j

    return data[i:len(beam)+i]

def _filter_stable_beams(data: pd.DataFrame, beam: pd.DataFrame) -> pd.DataFrame:
    # Merge the dfs on the timestamp
    data = pd.merge(data, beam, on=["runnum", "fillnum", "lsnum"], suffixes=("", "_beam")).drop_duplicates("timestampsec").reset_index(drop=True)

    # Filter by status
    data = data[data["status"] == b'STABLE BEAMS']

    # Drop all the duplicate collumns
    data = data.drop(data.filter(regex="_beam").columns, axis=1)

    return data

def _apply_lsmask(data: pd.DataFrame, ls_mask: str) -> pd.DataFrame:
    return data.query(ls_mask)

def _concatenate(dfs: List[pd.DataFrame]) -> pd.DataFrame:
    return pd.concat(dfs)

def _eliminate_duplicates(data: pd.DataFrame) -> pd.DataFrame:
    return data.drop_duplicates(subset="timestampsec")

def _aggregate_lumisections(data: pd.DataFrame) -> pd.DataFrame:
    return data.groupby(["runnum", "lsnum"]).agg(
        {
            "fillnum": "first",
            "timestampsec": "first",
            "egev": "first",
            "ncollidable": "first",
            "avgraw":"mean",
        }
    ).reset_index()

def _match_columns(data: pd.DataFrame, coefs: np.ndarray, node: str) -> pd.DataFrame:
    result = pd.DataFrame()

    # This column is in <run>:<fill> format
    result["#run:fill"] = data["runnum"].apply(lambda x: str(x)) + ":" + data["fillnum"].apply(lambda x: str(x))

    # This column is in <ls>:<ls> format
    result["ls"] = data["lsnum"].apply(lambda x: str(x) + ":" + str(x))

    # Some columns need to be converted to int
    result["time"] = data["timestampsec"].astype(int)

    # We only care about STABLE BEAMS
    result["beamstatus"] = "STABLE BEAMS"

    # All values from brilcalc appear to be rounded
    result["E(GeV)"] = np.ceil(data["egev"]).astype(int)

    result["delivered(hz/ub)"] = np.polyval(coefs, data["avgraw"] / data["ncollidable"]) * data["ncollidable"]
    result["recorded(hz/ub)"] = result["delivered(hz/ub)"]
    result["rateFromFile"] = data["avgraw"]

    # TODO: How should this be calculated?
    result["avgpu"] = 0.0

    result["source"] = _name_from_node(node)

    return result

def convert(paths: Dict[int, List[Tuple[Path,Path]]], normtag: NormTag, node: str) -> Iterable[pd.DataFrame]:
    """Convert the hd5 data to brilcalc csv format.

    Parameters
    ----------
    paths : Dict[int, List[Tuple[Path,Path]]]
        The mapping from run number to the hd5 file paths.
    normtag : NormTag
        The normtag to use for the conversion.
    node : str
        The node from which to read the data.

    Yields
    ------
    pd.DataFrame
        The converted data.
    """
    for run, paths in paths.items():
        if run not in normtag:
            logger.warning(f"Skipping run {run}. Not present in normtag.")
            continue

        logger.info(f"Processing run {run}")

        dfs: List[pd.DataFrame] = []
        for path, beam_path in paths:
            logger.info(f"Processing {path}")

            logger.info(f"Reading data")
            try:
                data = read_hd5(path, node)
            except NoSuchNodeError as err:
                logger.warning(f"No node {node} in {path}")
                continue
            logger.info(f"Reading beam")
            beam = read_hd5(beam_path, "beam")

            # Step a.
            logger.info(f"Filtering stable beams")
            try:
                data = _filter_stable_beams(data, beam)
            except ValueError as err:
                logger.warning(f"Could not filter stable beams: {err}")
                continue

            logger.info(f"Applying lsmask")
            data = _apply_lsmask(data, normtag.run_to_lsmask[run])

            dfs.append(data)

        if not dfs:
            logger.warning(f"No data for run {run}")
            continue

        # Step c.
        logger.info(f"Concatenating dataframes")
        data = _concatenate(dfs)

        # Step d.
        logger.info(f"Eliminating duplicates")
        data = _eliminate_duplicates(data)

        # Step e.
        logger.info(f"Aggregating lumisections")
        data = _aggregate_lumisections(data)

        # Step f.
        logger.info(f"Matching columns")
        data = _match_columns(data, normtag.run_to_iovtag[run].get_coefs(run), node)

        # Step g.
        yield data


def main() -> None:
    global logger

    parser = create_parser()
    args = parser.parse_args()

    agg = args.agg
    
    if args.verbose:
        level = logging.DEBUG
        np.set_printoptions(threshold=sys.maxsize)
    else:
        level = logging.INFO
    if args.log_to_file:
        logging.basicConfig(level=level, filename="h2b.log", filemode="a")
    else:
        logging.basicConfig(level=level)
    logger = logging.getLogger("h2b")

    if args.file and args.beam_file:
        input_type = InputType.FILE
    elif args.folder and args.beam_folder:
        input_type = InputType.FOLDER
    elif args.central and args.beam_central:
        input_type = InputType.CENTRAL
    else:
        logger.error("Invalid input")
        parser.print_help()
        sys.exit(1)

    normtag = NormTag(args.normtag)
    run_to_paths = compute_run_to_paths(input_type, args)

    # Filter out runs not present in the normtag
    run_to_paths = {run: paths for run, paths in run_to_paths.items() if run in normtag}

    desc = "Converting"
    total = len(run_to_paths)
    iterable = convert(run_to_paths, normtag, args.node)
    tqdm_object = tqdm(
        iterable=iterable,
        desc=desc,
        total=total,
        dynamic_ncols=True
    )

    datas: List[pd.DataFrame] = []
    for i, data in enumerate(tqdm_object):
        current_run = list(run_to_paths.keys())[i]
        tqdm_object.set_postfix(run=current_run, refresh=True)
        
        datas.append(data)

        if (i + 1) % agg == 0:
            pd.concat(datas).to_csv(args.output, index=False, mode="a", header=not args.output.exists())
            datas.clear()

    if len(datas) > 0:
        pd.concat(datas).to_csv(args.output, index=False, mode="a", header=not args.output.exists())

    if not args.output.exists():
        logger.warning("No data was found for the input you provided! Double check if the input was correct.")
    else:
        pd.read_csv(args.output)\
                .sort_values(by="time")\
                .reset_index(drop=True)\
                .to_csv(args.output, mode="w", index=False)


if __name__ == "__main__":
    main()

