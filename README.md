# H2B - Subset of `brilcalc lumi` tool

# **Disclaimer**

This repository is temporarie. This script will be moved to the [vdmtools](https://gitlab.cern.ch/flpereir/vdmtools) repo in the future!

# Quick Start

Check the help message in order to know how to use this tool:

```console
python h2b.py --help
```

# Example

Try this example command:

```console
python h2b.py --folder /brildata/23/8736/ --beam-folder /brildata/23/8736/ --node pltlumizero --normtag ../normtag_plt.json -o pltzero23v05.csv
```

An example of the output is [here](examples/pltzero23v05.csv).
